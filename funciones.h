// #pragma once;

/**
*@param int[] Vector a recorrer para evaluar el valor minimo
*@param int Tamaño del vector
*@returns int valor minimo
*/
int valorMinimo(int v[], int cant);

/**
*@param int[] Vector a recorrer para evaluar encontrar indice del valor maximo
*@param int Tamaño del vector
*@returns int indice donde se encuentra el valor minimo
*/
int ValorMaximo(int v[], int cant);

/**
*@param int[] Vector a recorrer para evaluar encontrar un valor buscado
*@param int Tamaño del vector
*@param int Valor buscado
*@returns int indice donde se encuentra el valor buscado o -1 en caso de no existir
*/
int BuscarValor(int v[], int cant, int buscado);

/**
*@param int[] Vector a cargar
*@param int Tamaño del vector
*/
void cargarVectorSinRepetidos(int v[], int tam);
