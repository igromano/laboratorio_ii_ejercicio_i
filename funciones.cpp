#include <iostream>
using namespace std;
#include "funciones.h"

//Punto 1
int valorMinimo(int v[], int cant){
    int i;
    int minimo =  v[0];
    for(i = 1; i < cant; i++){
        if(v[i] < minimo){
            minimo = v[i];
        }
    }

    return minimo;
}

//Punto 2
int ValorMaximo(int v[], int cant){
    int i, iMaximo = 0;
    int maximo = *v;
    for(i = 1; i < cant; i++){
        if(v[i] > maximo){
            maximo = v[i];
            iMaximo = i;
        }
    }
    return iMaximo;
}

//Punto 3
int BuscarValor(int v[], int cant, int buscado){
    int i;
    for(i = 0; i < cant; i++){
        if(v[i] == buscado){
            return i;
        }
    }
    return -1;
}

// Punto 4
void cargarVectorSinRepetidos(int v[], int tam)
{
    int i = 0, j, ingresado;
    bool repetido;

    while (i < tam){
        repetido = false;
        cout << endl << i + 1 << "- Ingresar un numero" << endl;
        cin >> ingresado;
        for (j = 0; j < tam; j++){
            if (v[j] == ingresado){
                cout << endl << "VALOR REPETIR, INGRESE OTRO NUMERO!" << endl;
                repetido = true;
                j = tam;
            }
        }
        if(!repetido){
            v[i] = ingresado;
            i++;
        }
    }

    for(j = 0; j < tam; j++){
        cout << " " << v[j] << " " ;
    }
}
